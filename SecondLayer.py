from GetHtml import HtmlAttr
from CompareKeywords import CompareWords
from DbInsert import DatabaseInserting

class SecondLayer(object):
    NeuronOne = 0
    NeuronTwo = 0
    def CalcNeutronOne(self, textStat, urlStat):
        if urlStat == 0:
            urlStat += 0.1
        avg = (textStat + urlStat) / 2
        self.NeuronOne += avg
        return self.NeuronOne
    def CalcNeutronTwo(self, descStat, keyStat, titleStat):
        if descStat == 0:
            descStat += 0.1
        if keyStat == 0:
            keyStat += 0.1
        if titleStat == 0:
            titleStat += 0.1
        avg = (descStat + keyStat + titleStat) / 3
        self.NeuronTwo += avg
        return self.NeuronTwo