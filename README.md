To start the program, enter in the command prompt "python $pathtofile.py" and then do whatever it prompt you for, you will need to have the http/s:// when the program prompts you for it.

Python needs to have these imported packages:
numpy
sqlite3
bs4
selenium

can use pip to install these (if you install python without opting to not install pip, it will already have pip ready for you. and it works like npm and the command would be "pip install $package")

Please don't edit the csv files in the WordLists directory or edit WebContent.db as it may not work.

More features like categorisation and branching out into other bad website topics will continue.