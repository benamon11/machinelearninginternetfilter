from bs4 import BeautifulSoup as bSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from DbInsert import DatabaseInserting
import time

class HtmlAttr(object):
    chrome_path = "C:\\Users\\BEvans\\Desktop\\ChromeDriver\\chromedriver.exe"
    driver = webdriver.Chrome(chrome_path)
    di = DatabaseInserting("WebContent.db", "HtmlContent")
    def __init__(self, raw_url):
        self.raw_url = raw_url
        self.driver.get(raw_url)
        time.sleep(5)
        self.raw_text = self.driver.find_element_by_tag_name("html").text.replace('"', "'")
        try:
            self.raw_meta_description = self.driver.find_element_by_xpath("//meta[@name='description']").get_attribute("content").replace('"', "'")
        except:
            self.raw_meta_description = ""
        try:
            self.raw_meta_keywords = self.driver.find_element_by_xpath("//meta[@name='keywords']").get_attribute("content").replace('"', "'")
        except:
            self.raw_meta_keywords = ""
        self.raw_title = self.driver.find_element_by_xpath("/html/head/title").get_attribute("innerHTML").replace('"', "'")
        self.raw_a_tags = self.raw_a_tagMethod()
    def raw_a_tagMethod(self):
        raw_body = self.driver.find_element_by_tag_name("html").get_attribute("innerHTML").replace('"', "'")
        soupBody = bSoup(raw_body, "html.parser")
        aHrefs = []
        try:
            for a in soupBody.find_all('a', href=True):
                aHrefs.append(a['href'])
        except:
            return aHrefs
        return aHrefs #"*".join(aHrefs)
    def insert_into_database(self):
        self.di.InsertRecord(self.raw_url, self.raw_text, self.raw_meta_description, self.raw_meta_keywords, self.raw_title, self.raw_a_tags)
    def close(self):
        self.driver.close()