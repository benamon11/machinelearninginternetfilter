from GetHtml import HtmlAttr
from CompareKeywordsRetry import CompareWords
from UpdatingBadWords import UpdatingBadWords
from SecondLayer import SecondLayer
from ThirdLayer import ThirdLayer
from DbInsert import DatabaseInserting
from UpdatingBadWords import UpdatingBadWords

print("Please input url")
user_url = input()


CollectingData = HtmlAttr(user_url)
Database = DatabaseInserting("WebContent.db", "HtmlContent")
WebsiteData = Database.ExtractAll()
#Comparing Words
Compare = CompareWords(user_url)

CompareTextNeuron = Compare.txtPts
print(CompareTextNeuron)
CompareDescNeuron = Compare.mDescPts
print(CompareDescNeuron)
CompareKeyNeuron = Compare.mKeyPts
print(CompareKeyNeuron)
CompareTitleNeuron = Compare.titlePts
print(CompareTitleNeuron)
CompareATagsNeuron = Compare.urlPts
print(CompareATagsNeuron)
SL = SecondLayer()
TL = ThirdLayer()
TL.Determiner(SL.CalcNeutronOne(CompareTextNeuron, CompareATagsNeuron), SL.CalcNeutronTwo(CompareDescNeuron, CompareKeyNeuron, CompareTitleNeuron))

#If it's a bad website
if TL.FinalNeuron > 40.0:
    Update = UpdatingBadWords(user_url)
    isBad = Database.CheckIfUrlMatch(user_url)

    Update.EnterInCSV()
    Update.RemoveUniqueValuesInCSV()
    #Database.InsertRecord(CollectingData.domain, CollectingData.content, CollectingData.description, CollectingData.keywords, CollectingData.title, CollectingData.atags)
    print("Bad Website, updated records")
else:
    print("Not a bad website")

CollectingData.close()