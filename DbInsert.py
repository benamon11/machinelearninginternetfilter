import sqlite3


class DatabaseInserting(object):

    def __init__(self, dbName, tabName):
        self.dbName = dbName
        self.tableName = tabName
        with sqlite3.connect(f"{dbName}") as self.db:
            self.cursor = self.db.cursor()
        self.cursor.execute(f'''CREATE TABLE IF NOT EXISTS {self.tableName}(siteID INTEGER PRIMARY KEY, domain VARCHAR(30), content VARCHAR(500), description VARCHAR(100), keywords VARCHAR(100), title VARCHAR(50), atags VARCHAR(50));''')
        
    def InsertRecord(self, domain, content, description, keywords, title, atags):
        self.cursor.execute(f"""INSERT INTO {self.tableName}(domain, content, description, keywords, title, atags) VALUES ("{domain}", "{content}", "{description}", "{keywords}", "{title}", "{atags}");""")
        self.db.commit()

    def ExtractAll(self):
        self.cursor.execute(f"""SELECT * FROM {self.tableName};""")
        records = self.cursor.fetchall()
        return records

    def CheckIfUrlMatch(self, url):
        self.cursor.execute(f"SELECT * FROM {self.tableName} WHERE domain = ?", [(url)])
        if self.cursor.fetchall():
            return True