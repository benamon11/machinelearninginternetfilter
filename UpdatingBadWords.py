from GetHtml import HtmlAttr
from DbInsert import DatabaseInserting
from RemoveGenericWords import Remove

class UpdatingBadWords(object):
    f = open("./WordLists/edgeCases.csv", "r")
    blackList = f.read().replace(" ", "").split(",")
    badWords = []
    badLinks = []
    def __init__(self, raw_url):
        self.rmv = Remove()
        self.raw_url = raw_url
        self.ha = HtmlAttr(self.raw_url)
        self.db = DatabaseInserting("WebContent.db", "HtmlContent")
        self.DatabaseContent = self.db.ExtractAll()

        for entry in self.DatabaseContent:
            self.CompareText(entry[2])
            self.CompareMetaDesc(entry[3])
            self.CompareMetaKey(entry[4])
            self.CompareTitle(entry[5])
            self.CompareaTags(entry[6])
        

    def CompareText(self, checker_text):
        suspect_text = self.ha.raw_text.lower()
        suspect_words = suspect_text.split()
        checker_words = checker_text.split()
        for sword in suspect_words:
            for cword in checker_words:
                if sword in cword and sword not in self.blackList and not sword.isdigit():
                    self.badWords.append(self.rmv.RemoveGenericWords(sword))

    def CompareMetaDesc(self, checker_desc):
        suspect_desc = self.ha.raw_meta_description.lower()
        suspect_words = suspect_desc.lower().split()
        checker_words = checker_desc.split()
        for sword in suspect_words:
            for cword in checker_words:
                if sword in cword and sword not in self.blackList and not sword.isdigit():
                    self.badWords.append(self.rmv.RemoveGenericWords(sword))

    def CompareMetaKey(self, checker_key):
        suspect_key = self.ha.raw_meta_keywords.lower()
        suspect_words = suspect_key.split()
        checker_words = checker_key.split()
        for sword in suspect_words:
            for cword in checker_words:
                if sword in cword and sword not in self.blackList and not sword.isdigit():
                    self.badWords.append(self.rmv.RemoveGenericWords(sword))

    def CompareTitle(self, checker_title):
        suspect_title = self.ha.raw_title.lower()
        suspect_words = suspect_title.split()
        checker_words = checker_title.split()
        for sword in suspect_words:
            for cword in checker_words:
                if sword in cword and sword not in self.blackList and not sword.isdigit():
                    self.badWords.append(self.rmv.RemoveGenericWords(sword))

    def CompareaTags(self, checker_urls):
        suspect_urls = self.ha.raw_a_tags
        for surl in suspect_urls:
            if checker_urls in surl or surl in checker_urls:
                self.badLinks.append(self.rmv.RemoveGenericWords(surl))

    def EnterInCSV(self):
        BadWordsFile = open("./WordLists/badWords.csv", "a")
        for bw in set(self.badWords):
            BadWordsFile.write(bw + ",")
        BadWordsFile.close()
        BadLinksFile = open("./WordLists/badLinks.csv", "a")
        for bl in set(self.badLinks):
            BadLinksFile.write(bl + ",")
        BadLinksFile.close()

    def RemoveUniqueValuesInCSV(self):
        BadWordsFileRead = open("./WordLists/badWords.csv", "r")
        CSVContent = set(BadWordsFileRead.read().split(","))
        BadWordsFileRead.close()
        BadWordsFileWrite = open("./WordLists/badWords.csv", "w")
        ListOfWords = ""
        for word in CSVContent:
            if word == "":
                continue
            else:
                ListOfWords = ListOfWords + word + ","
        BadWordsFileWrite.write(ListOfWords)
        BadWordsFileWrite.close()

        BadLinksFileRead = open("./WordLists/badLinks.csv", "r")
        CSVContent = set(BadLinksFileRead.read().split(","))
        BadLinksFileRead.close()
        BadLinksFileWrite = open("./WordLists/badLinks.csv", "w")
        ListOfWords = ""
        for word in CSVContent:
            if word == "":
                continue
            else:
                ListOfWords = ListOfWords + word + ","
        BadLinksFileWrite.write(ListOfWords)
        BadLinksFileWrite.close()