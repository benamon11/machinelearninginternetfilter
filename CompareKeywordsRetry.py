from GetHtml import HtmlAttr
from DbInsert import DatabaseInserting
from RemoveGenericWords import Remove

class CompareWords(object):
    txtPts = 0.0
    mDescPts = 0.0
    mKeyPts = 0.0
    titlePts = 0.0
    urlPts = 0.0
    edgeCase = open("./WordLists/edgeCases.csv", "r")
    blackList = edgeCase.read().replace(" ", "").split(",")
    badWords = open("./WordLists/badWords.csv", "r").read().split(",")
    badLinks = open("./WordLists/badLinks.csv", "r").read().split(",")
    def __init__(self, raw_url):
        self.raw_url = raw_url
        self.ha = HtmlAttr(self.raw_url)
        self.txtPts += self.CompareText(self.badWords)
        self.mDescPts += self.CompareMetaDesc(self.badWords)
        self.mKeyPts += self.CompareMetaKey(self.badWords)
        self.titlePts += self.CompareTitle(self.badWords)
        self.urlPts += self.CompareaTags(self.badLinks)


    def CompareText(self, checker_list):
        suspect_text = self.ha.raw_text.lower()
        suspect_words = suspect_text.split()
        for sword in suspect_words:
            for cword in checker_list:
                if sword == cword and sword not in self.blackList and not sword.isdigit():
                    print(sword)
                    self.txtPts += 0.1
        return (self.txtPts / len(suspect_words) * 1000)

    def CompareMetaDesc(self, checker_list):
        suspect_desc = self.ha.raw_meta_description.lower()
        suspect_words = suspect_desc.lower().split()
        for sword in suspect_words:
            for cword in checker_list:
                if sword == cword and sword not in self.blackList and not sword.isdigit():
                    self.mDescPts += 0.3
        try:
            return (self.mDescPts / len(suspect_words) * 100)
        except:
            return 0

    def CompareMetaKey(self, checker_list):
        suspect_key = self.ha.raw_meta_keywords.lower()
        suspect_words = suspect_key.split()
        for sword in suspect_words:
            for cword in checker_list:
                if sword == cword and sword not in self.blackList and not sword.isdigit():
                    self.mKeyPts += 0.3
        if self.mKeyPts > 0:
            return (self.mKeyPts / len(suspect_words) * 100)
        else:
            return 0

    def CompareTitle(self, checker_list):
        suspect_title = self.ha.raw_title.lower()
        suspect_words = suspect_title.split()
        for sword in suspect_words:
            for cword in checker_list:
                if sword == cword and sword not in self.blackList and not sword.isdigit():
                    self.titlePts += 0.2
        return (self.titlePts / len(suspect_words) * 100)

    def CompareaTags(self, checker_urls):
        suspect_urls = self.ha.raw_a_tags
        for surl in suspect_urls:
            for curl in checker_urls:
                if curl == surl or surl in curl:
                    self.urlPts += 0.25
        if self.urlPts > 0:
            return (self.urlPts / len(suspect_urls) * 100)
        else:
            return 0